// require('dotenv').config();
import React from 'react';
// import axios from 'axios';
// import dotenv from 'dotenv';
// const fast2sms = require('fast-two-sms');
import { useState } from 'react';
import { Box, Textarea, Grid, Text, Button } from '@chakra-ui/react';
import { FormControl, FormLabel, Input, GridItem } from '@chakra-ui/react';

function Receipt() {
  const [currentDate, setCurrentDate] = useState(new Date());

  const [user, setUser] = useState({
    name: '',
    phone: '',
    address: '',
    particulars: '',
    quantity1: '',
    quantity2: '',
    amount1: '',
    amount2: '',
    advance: '',
    balance: '',
    delivery: '',
    total: '',
  });

  const handleInput = (e) => {
    console.log(e);
    let name = e.target.name;
    let value = e.target.value;

    setUser({
      ...user,
      [name]: value,
    });
  };

  React.useEffect(() => {
    const timer = setInterval(() => {
      setCurrentDate(new Date());
    }, 1000);
    return () => clearInterval(timer);
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(user);
    try {
      const response = await fetch(`https://localhost:5000/register`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Headers': 'Authorization',
        },
        body: JSON.stringify(user),
      });

      if (response.ok) {
        setUser({
          name: '',
          phone: '',
          address: '',
          particulars: '',
          quantity1: '',
          quantity2: '',
          amount1: '',
          amount2: '',
          advance: '',
          balance: '',
          delivery: '',
          total: '',
        });
      }
      console.log(response);
      alert('SUCCESSFULL');
    } catch (error) {
      console.log('From the Register', error);
    }
  };

  return (
    <>
      <Box
        margin='15px'
        border='1px solid black'
        padding='2rem'
        paddingTop='30px'
        paddingBottom='60px'
        borderRadius='10px'
        onSubmit={handleSubmit}>
        <Text
          display='flex'
          justifyContent='space-between'
          fontFamily='sans-serif'
          fontSize='xx-large'
          textAlign='center'
          padding='10px'>
          <strong>Receipt</strong>
        </Text>

        {/* Time */}
        <Box
          fontSize='20px'
          fontFamily='sans-serif'
          marginBottom='10px'
          align='end'>
          <Text>
            <strong>Date: {currentDate.toLocaleDateString()}</strong>
          </Text>
          <Text>
            <strong>Time: {currentDate.toLocaleTimeString()}</strong>
          </Text>
        </Box>

        {/* FORM CONTROL */}
        <FormControl isRequired>
          {/* Name */}
          <FormLabel>Name:</FormLabel>
          <Input
            name='name'
            placeholder=' Name'
            marginBottom='20px'
            border='1px solid grey'
            autoComplete='off'
            value={user.name}
            onChange={handleInput}
          />
        </FormControl>

        {/* Number */}
        <FormControl isRequired>
          <FormLabel>Contact No. :</FormLabel>
          <Input
            marginBottom='20px'
            border='1px solid grey'
            placeholder='+91'
            name='phone'
            autoComplete='on'
            value={user.phone}
            onChange={handleInput}
          />
        </FormControl>
        {/* Address */}
        <FormControl>
          <FormLabel>Address:</FormLabel>
          <Input
            name='address'
            placeholder='Address'
            autoComplete='off'
            marginBottom='20px'
            border='1px solid grey'
            value={user.address}
            onChange={handleInput}
          />
        </FormControl>

        {/* Particulars */}
        <Grid templateColumns='repeat(8, 3fr)' gap={2}>
          <GridItem colSpan={5} h='10'>
            <FormControl isRequired>
              <FormLabel>Particulars:</FormLabel>
              <Textarea
                name='particulars'
                placeholder='Enter Item Name'
                border='1px solid grey'
                boxSize='415px'
                width='100%'
                value={user.particulars}
                onChange={handleInput}
              />
            </FormControl>
          </GridItem>

          {/* Quantity */}
          <GridItem colStart={6} colEnd={6} h='2'>
            <FormControl isRequired>
              <FormLabel>Quantity:</FormLabel>
              <Input
                marginBottom='20px'
                border='1px solid grey'
                placeholder='1'
                name='quantity1'
                value={user.quantity1}
                onChange={handleInput}
              />
            </FormControl>
          </GridItem>

          {/* Amount */}
          <GridItem colStart={7} colEnd={8} h='4' maxWidth='100%'>
            <FormControl isRequired>
              <FormLabel>Amount:</FormLabel>
              <Input
                marginBottom='20px'
                border='1px solid grey'
                placeholder='INR'
                name='amount1'
                value={user.amount1}
                onChange={handleInput}
              />
            </FormControl>
          </GridItem>

          {/* Quantity 2 */}
          <GridItem colStart={6} colEnd={6} h='4' marginTop='30px'>
            <Input
              marginBottom='20px'
              border='1px solid grey'
              placeholder='2'
              name='quantity2'
              value={user.quantity2}
              onChange={handleInput}
            />
          </GridItem>

          {/* Amount 2 */}
          <GridItem colStart={7} colEnd={8} h='4' marginTop='30px'>
            <Input
              marginBottom='20px'
              border='1px solid grey'
              placeholder='INR'
              name='amount2'
              value={user.amount2}
              onChange={handleInput}
            />
          </GridItem>

          {/* Advance */}
          <GridItem colStart={6} colEnd={9} h='10' marginTop='25px'>
            <FormControl>
              <FormLabel>
                Advance:
                <Input
                  placeholder='Advance Amt.'
                  width='100%'
                  border='1px solid grey'
                  name='advance'
                  value={user.advance}
                  onChange={handleInput}
                />
              </FormLabel>
            </FormControl>
          </GridItem>

          {/* Balance */}
          <GridItem colStart={6} colEnd={9} h='10' zIndex='999'>
            <FormControl marginTop='20px'>
              <FormLabel>
                Balance:
                <Input
                  placeholder='Balance Amt.'
                  width='100%'
                  border='1px solid grey'
                  name='balance'
                  value={user.balance}
                  onChange={handleInput}
                />
              </FormLabel>
            </FormControl>
          </GridItem>

          {/* Delivery Charges */}
          <GridItem colStart={6} colEnd={9} h='10' zIndex='99'>
            <FormControl marginTop='40px'>
              <FormLabel>
                Delivery:
                <Input
                  placeholder='Delivery Charges'
                  width='100%'
                  border='1px solid grey'
                  name='delivery'
                  value={user.delivery}
                  onChange={handleInput}
                />
              </FormLabel>
            </FormControl>
          </GridItem>

          {/* Total */}
          <GridItem colStart={6} colEnd={9} h='10'>
            <FormControl paddingTop='60px'>
              <FormLabel>
                Total:
                <Input
                  marginBottom='100px'
                  border='1px solid grey'
                  placeholder='Total Amount'
                  name='total'
                  value={user.total}
                  onChange={handleInput}
                />
              </FormLabel>
            </FormControl>
          </GridItem>

          {/* Button */}
          <GridItem
            colStart={6}
            colEnd={9}
            h='2'
            paddingTop='90px'
            paddingBottom='10px'>
            <Button
              onClick={handleSubmit}
              minWidth='100%'
              bg='green.300'
              _hover={{ bgColor: 'green', color: 'white' }}>
              Submit
            </Button>
          </GridItem>
        </Grid>
        {/* </form> */}
      </Box>
    </>
  );
}

export default Receipt;
